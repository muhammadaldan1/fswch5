const express = require('express')
const app = express()
const port = 3000
app.use(express.static('assets'))
app.use(express.static('public'))
app.set('view engine', 'ejs') 


app.get("/", (req, res) => {
    res.render("index");
  });

app.get("/game", (req, res) => {
    res.render("game");
  });

app.get("/user", (req,res) =>{
  const data = require('./data/user.json')
  res.send(data)
})

app.use((req,res) => {
  res.status(404).render("404")
})

app.use((err,req,res,next) =>{
  res.status(500).render("500",{error: err.message})
})

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
  });